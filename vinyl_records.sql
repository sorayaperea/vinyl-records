create database vinyl_records
go

use vinyl_records 
go


-- TABLA DIRECCION
create table direccion
(
 codigoPostal varchar(20),
 estado varchar(50) not null,
 municipio varchar(50) not null,
 colonia varchar(100), 
 callePrincipal varchar(50),
 numeroExterior varchar(50),
 calle1 varchar(50),
 calle2 varchar(50)
)
go


-- TABLA USUARIO
create table usuario
(
 usId int primary key identity(1,1),
 usNombre varchar(50) not null,
 usApellido varchar(80) not null,
 usFechaNacimiento datetime,
 usCorreo varchar(100),
 usCelular varchar(50)
)
go


-- TABLA METODO DE PAGO
create table metodoPago
(
 metId int primary key identity(1,1),
 metNombre varchar(50) not null
)
go


-- TABLA PEDIDO
create table pedido
(
 pedId int primary key identity(1,1),
 pedFecha datetime not null,
 pedStatus varchar(100),
 pedTotal money,
 pedUsuario int not null,
 pedMetodoPago int not null,
 foreign key (pedUsuario) references usuario(usId),
 foreign key (pedMetodoPago) references metodoPago(metId)
)
go


-- TABLA GENERO
create table genero
(
 genId int primary key identity(1,1),
 genNombre varchar(50) not null,
 genDescripcion varchar(100)
)
go


-- TABLA ARTISTA
create table artista
(
 artId int primary key identity(1,1),
 artNombre varchar(100) not null
)
go


-- TABLA PRODUCTO
create table producto
(
 prodId varchar(10) primary key,
 prodNombre varchar(100) not null,
 prodDescripcion varchar(100),
 prodPrecio money,
 prodGenero int not null,
 prodArtista int not null,
 foreign key (prodGenero) references genero(genId),
 foreign key (prodArtista) references artista(artId)
)
go



-- TABLA PROD - PEDIDO
create table prodPedido
(
 ppPedido int not null,
 ppProducto varchar(10) not null,
 ppCantidad int not null,
 ppImporte money,
 foreign key (ppPedido) references pedido(pedId),
 foreign key (ppProducto) references producto(prodId)
)
go

alter table prodPedido			
add constraint PK_prodPedido   
primary key(ppPedido,ppProducto)
go

